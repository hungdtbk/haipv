package com.example.tests.haipv.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.example.tests.haipv.R;
import com.example.tests.haipv.common.BaseActivity;
import com.example.tests.haipv.customview.MyTabLayout;
import com.example.tests.haipv.data.entity.Player;
import com.example.tests.haipv.data.entity.Topic;
import com.example.tests.haipv.dialog.selecttopic.SelectTopicDialogFragment;
import com.example.tests.haipv.ui.main.detail.DetailFragment;
import com.example.tests.haipv.ui.main.list.ListFragment;
import com.example.tests.haipv.utils.LogUtils;
import com.example.tests.haipv.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MyTabLayout.OnTabSelected {

    MainViewModel viewModel;
    @BindView(R.id.tab_layout)
    MyTabLayout tabLayout;
    @BindView(R.id.fl_fragment_list_container)
    FrameLayout flFragmentContainer;
    private List<Topic> listTopics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        listTopics = Utils.getListTopicsFromServer();
        subscribeData();
        initListener();
    }

    private void initListener() {
        tabLayout.setListener(this);
    }

    private void subscribeData() {
        viewModel.currentTopic.observe(this, topic -> {
            LogUtils.logD("MainActivity", "onChanged topic: ", topic);
            onTopicChanged();
        });
        viewModel.currentTopic.setValue(listTopics.get(0));
    }

    @Override
    public void onTabSelected(View view, int position) {
        switch (position) {
            case 0:
            case 1:
                tabLayout.setSelectedTab(position);
                viewModel.currentTopic.setValue(listTopics.get(position));
                break;
            case 2:
                showDialogSelectTopic(listTopics.subList(2, listTopics.size()));
        }
    }

    private void showDialogSelectTopic(List<Topic> topics) {
        viewModel.listTopicsToSelect = topics;
        SelectTopicDialogFragment dialogFragment = new SelectTopicDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "");
        dialogFragment.setListener(topic -> {
            tabLayout.setSelectedTab(2);
            tabLayout.setTitleTab3(topic.title);
            viewModel.currentTopic.setValue(topic); //onTopicChanged sẽ được gọi tự động sau khi chạy dòng lệnh này
        });
    }

    private void onTopicChanged() {
        replaceFragment(R.id.fl_fragment_list_container, new ListFragment());
    }

    public void showDetail(Player player) {
        viewModel.player = player;
        addFragment(R.id.fl_fragment_detail_container, new DetailFragment());
    }
}
