package com.example.tests.haipv.data.entity;

public class Item {
    public String title;
    public boolean isChecked;

    public Item() {
    }

    public Item(String title) {
        this.title = title;
    }
}
