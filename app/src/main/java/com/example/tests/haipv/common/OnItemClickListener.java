package com.example.tests.haipv.common;

public interface OnItemClickListener {
    public void onItemClick(int position);
}
