package com.example.tests.haipv.dialog.selecttopic;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tests.haipv.R;
import com.example.tests.haipv.common.OnItemClickListener;
import com.example.tests.haipv.data.entity.Topic;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectTopicAdapter extends RecyclerView.Adapter<SelectTopicAdapter.ViewHolder> {
    private final List<Topic> listTopicsToSelect;

    public OnItemClickListener listener;

    public SelectTopicAdapter(List<Topic> listTopicsToSelect) {
        this.listTopicsToSelect = listTopicsToSelect;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic_to_select, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return listTopicsToSelect.size();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(int position) {
            Topic topic = listTopicsToSelect.get(position);
            tvTitle.setText(topic.title);
            tvTitle.setOnClickListener(view -> listener.onItemClick(position));
        }
    }
}
