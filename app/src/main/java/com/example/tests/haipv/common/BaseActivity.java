package com.example.tests.haipv.common;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.tests.haipv.R;

public abstract class BaseActivity extends AppCompatActivity {
    public void replaceFragment(int containerId, Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }

    public void addFragment(int containerId, Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(containerId, fragment)
                .addToBackStack("")
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }
}
