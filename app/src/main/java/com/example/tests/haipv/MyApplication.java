package com.example.tests.haipv;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import io.paperdb.Paper;

public class MyApplication extends MultiDexApplication {
    private static MyApplication instance;

    public static MyApplication getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();

        instance = this;
        Paper.init(this);
    }
}
