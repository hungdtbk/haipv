package com.example.tests.haipv.data.entity;

public class Topic {
    public int id;
    public String title;

    public Topic() {
    }

    public Topic(int id, String title) {
        this.id = id;
        this.title = title;
    }
}
