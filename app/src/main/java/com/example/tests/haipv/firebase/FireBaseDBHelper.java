package com.example.tests.haipv.firebase;

import android.support.annotation.NonNull;

import com.example.tests.haipv.common.ApiListener;
import com.example.tests.haipv.common.Constants;
import com.example.tests.haipv.data.entity.Player;
import com.example.tests.haipv.data.entity.Topic;
import com.example.tests.haipv.utils.DateUtil;
import com.example.tests.haipv.utils.LogUtils;
import com.example.tests.haipv.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hungdt on 3/16/2018
 */

public class FireBaseDBHelper {

    private static FireBaseDBHelper instance;
    public FirebaseFirestore db;

    public FireBaseDBHelper() {
        db = FirebaseFirestore.getInstance();
    }

    public static FireBaseDBHelper getInstance() {
        if (instance == null) {
            instance = new FireBaseDBHelper();
        }
        return instance;
    }

    public static void getListPlayersToday(ApiListener<List<Player>> listener) {
        String field = Player.FIELD_SCORE_BY_DATES + "." + DateUtil.getTodayString();
        FireBaseDBHelper.getInstance().db.collection(Constants.FB_COLLECTION_PLAYERS)
                .orderBy(field, Query.Direction.DESCENDING)
                .startAt()
                .limit(100).get().addOnCompleteListener(task -> listener.onSuccess(Utils.convertListPlayersFromDocumentTask(task)));
    }

    public static void getListPlayersYesterday(ApiListener<List<Player>> listener) {
        String field = Player.FIELD_SCORE_BY_DATES + "." + DateUtil.getYesterdayString();
        FireBaseDBHelper.getInstance().db.collection(Constants.FB_COLLECTION_PLAYERS)
                .orderBy(field, Query.Direction.DESCENDING)
                .limit(100).get().addOnCompleteListener(task -> listener.onSuccess(Utils.convertListPlayersFromDocumentTask(task)));
    }

    public static void getListPlayersWeek(ApiListener<List<Player>> listener) {
        String field = Player.FIELD_SCORE_BY_DATES + "." + DateUtil.getThisWeekString();
        LogUtils.logD("MainModel getListPlayersWeek", field);
        FireBaseDBHelper.getInstance().db.collection(Constants.FB_COLLECTION_PLAYERS)
                .orderBy(field, Query.Direction.DESCENDING)
                .limit(100).get().addOnCompleteListener(task -> listener.onSuccess(Utils.convertListPlayersFromDocumentTask(task)));
    }

    public static void getDataFromServer(Topic topic, DocumentSnapshot lastVisible, ApiListener<List<DocumentSnapshot>> listener) {
        Query command = getQueryCommandByTopic(topic);
        if (lastVisible != null) {
            command = command.startAfter(lastVisible);
        }
        command.get().addOnCompleteListener(task
                -> listener.onSuccess(Utils.convertListDocumentSnapshotsFromDocumentTask(task)));
    }

    @NonNull
    private static Query getQueryCommandByTopic(Topic topic) {
        String field;
        if (topic.id == 0) {
            field = Player.FIELD_SCORE_BY_DATES + "." + DateUtil.getTodayString();
        } else if(topic.id == 2){
            field = Player.FIELD_SCORE_BY_DATES + "." + DateUtil.getYesterdayString();
        } else {
            field = Player.FIELD_SCORE_BY_DATES + "." + DateUtil.getThisWeekString();
        }
        return getInstance().db.collection(Constants.FB_COLLECTION_PLAYERS)
                .orderBy(field, Query.Direction.DESCENDING)
                .limit(20);
    }
}
