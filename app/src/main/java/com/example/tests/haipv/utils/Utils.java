package com.example.tests.haipv.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.example.tests.haipv.MyApplication;
import com.example.tests.haipv.data.entity.Player;
import com.example.tests.haipv.data.entity.Topic;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

public class Utils {
    public static Topic getDefaultTopic() {
        return new Topic(1, "default tp");
    }

    public static List<Topic> getListTopicsFromServer() {
        //fake data
        List<Topic> topics = new ArrayList<>();
        int id = 0;
        topics.add(new Topic(id++, "TOPIC 1"));
        topics.add(new Topic(id++, "TOPIC 2"));
        topics.add(new Topic(id++, "TOPIC 3"));
        topics.add(new Topic(id++, "TOPIC 4"));
        topics.add(new Topic(id++, "TOPIC 5"));
        topics.add(new Topic(id++, "TOPIC 6"));
        topics.add(new Topic(id++, "TOPIC 7"));
        topics.add(new Topic(id++, "TOPIC 8"));
        return topics;
    }

    private static String sDeviceId;

    public static boolean isAppPurchased() {
        return false;
    }

    public static void takeScreenShotAndShare(final Context context, View view, boolean incText, String text) {
        try {

            File mPath = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "screenshot.png");
            //File imageDirectory = new File(mPath, "screenshot.png");

            view.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
            view.setDrawingCacheEnabled(false);

            FileOutputStream fOut = new FileOutputStream(mPath);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, fOut);
            fOut.flush();
            fOut.close();

            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            Uri pictureUri = Uri.fromFile(mPath);
            shareIntent.setType("image/*");
            if (incText) {
                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
            }
            shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(shareIntent, "Share image using"));
        } catch (Throwable tr) {
            Log.d(TAG, "Couldn't save screenshot", tr);
        }

    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId() {
        if (TextUtils.isEmpty(sDeviceId)) {
            sDeviceId = Settings.Secure.getString(MyApplication.getInstance()
                    .getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return sDeviceId;
    }

    public static int getScoreByDate(HashMap<String, Integer> scoreByDates, String key) {
        try {
            Integer score = scoreByDates.get(key);
            if (score != null) {
                return score;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static List<Player> convertListPlayersFromDocumentSnapshot(List<DocumentSnapshot> result) {
        List<Player> players = new ArrayList<>();
        for (DocumentSnapshot document : result) {
            try {
                players.add(document.toObject(Player.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return players;
    }

    @NonNull
    public static List<Player> convertListPlayersFromDocumentTask(Task<QuerySnapshot> task) {
        List<Player> players = new ArrayList<>();
        if (task.isSuccessful()) {
            for (QueryDocumentSnapshot document : task.getResult()) {
                try {
                    players.add(document.toObject(Player.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return players;
    }

    @NonNull
    public static List<DocumentSnapshot> convertListDocumentSnapshotsFromDocumentTask(Task<QuerySnapshot> task) {
        List<DocumentSnapshot> documentSnapshots = new ArrayList<>();
        if (task.isSuccessful()) {
            for (QueryDocumentSnapshot document : task.getResult()) {
                try {
                    documentSnapshots.add(document);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return documentSnapshots;
    }

    public static void addListPlayersToCompare(Map<String, Player> listPlayersToCompare, List<Player> result) {
        for (Player player : result) {
            listPlayersToCompare.put(player.id, player);
        }
    }

    public static List<Player> getPlayersFromMap(Map<String, Player> mapOfPlayersToCompare) {
        return new ArrayList<>(mapOfPlayersToCompare.values());
    }

//    @SuppressLint("CheckResult")
//    public static void getListRawPlayers(List<Player> listPlayers, int filterType, Consumer<List<RawPlayer>> consumer) {
//        Single.create((SingleOnSubscribe<List<RawPlayer>>) emitter -> {
//            List<RawPlayer> rawPlayers = getListRawPlayers(listPlayers, filterType);
//            emitter.onSuccess(rawPlayers);
//        }).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(consumer);
//    }
//
//    private static List<RawPlayer> getListRawPlayers(List<Player> listPlayers, int filterType) {
//        List<RawPlayer> rawPlayers = new ArrayList<>();
//        String keyToGetValue;
//        switch (filterType) {
//            case FilterType.TODAY:
//                keyToGetValue = DateUtil.getTodayString();
//                break;
//            case FilterType.YESTERDAY:
//                keyToGetValue = DateUtil.getYesterdayString();
//                break;
//            case FilterType.WEEK:
//                keyToGetValue = DateUtil.getThisWeekString();
//                break;
//            case FilterType.MONTH:
//                keyToGetValue = DateUtil.getThisMonthString();
//                break;
//            default:
//                keyToGetValue = DateUtil.getTodayString();
//        }
//        for (Player player : listPlayers) {
//            int score = player.scoreByDates.get(keyToGetValue);
//            RawPlayer rawPlayer = new RawPlayer(player.id, player.name, score);
//            rawPlayers.add(rawPlayer);
//        }
//        Collections.sort(rawPlayers, Collections.reverseOrder());
//        return rawPlayers;
//    }
//
//    public static boolean isValidPlayerName(String name) {
//        return name.replaceAll("\\s", "").length() >= Constants.Configs.MIN_PLAYER_NAME_LENGTH;
//    }
//
//    public static int getTodayRank(String deviceId, List<RawPlayer> playersToday) {
//        for (int i = 0; i < playersToday.size(); i++) {
//            if (playersToday.get(i).id.equals(deviceId)) {
//                return i + 1;
//            }
//        }
//        return playersToday.size() + 1;
//    }

    public static int getRankingRewardedForRank(int rank) {
        if (rank == 1) {
            return 300;
        } else if (rank == 2) {
            return 280;
        } else if (rank == 3) {
            return 260;
        } else if (rank == 4) {
            return 255;
        } else if (rank == 5) {
            return 250;
        } /*else if (rank > 5 && rank <= 10) {
            return 140;
        } else if (rank > 10 && rank <= 20) {
            return 100;
        } else if (rank > 20 && rank <= 50) {
            return

        } */ else if (rank > 5 && rank <= 100) {
            return 50 + (100 - rank) * 2;
        }
        return 50;
    }


//    public static Player getCurrentPlayer() {
//        try {
//            return Paper.book().read(Constants.KEY_PLAYER, new Player());
//        } catch (Exception e) {
//            return new Player();
//        }
//    }
}
