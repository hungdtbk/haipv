package com.example.tests.haipv.ui.main.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.tests.haipv.R;
import com.example.tests.haipv.common.OnItemClickListener;
import com.example.tests.haipv.data.entity.Player;
import com.example.tests.haipv.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by hungdt on 3/19/2018
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    List<Player> players = new ArrayList<>();
    public OnItemClickListener listener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_ranking, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogUtils.logD("ListAdapter", "onBindViewHolder 42: ");
        holder.bindView(position);

    }

    @Override
    public int getItemCount() {
        LogUtils.logD("ListAdapter", "getItemCount 50: ", players.size());
        return players.size();
    }

    public void updateData(List<Player> players) {
        LogUtils.logD("ListAdapter", "updateData : ", players.size());
        this.players = players;
        notifyDataSetChanged();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_rank)
        TextView tvRank;
        @BindView(R.id.tv_player_name)
        TextView tvPlayerName;
        @BindView(R.id.tv_score)
        TextView tvScore;
        @BindView(R.id.img_top_rank)
        ImageView imgTopRank;
        @BindView(R.id.ll_item_ranking_container)
        LinearLayout llItemRankingContainer;
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }

        public void bindView(int position) {
            LogUtils.logD("ViewHolder", "bindView 75: ");
            Player player = players.get(position);
            setTextForRank(tvRank, imgTopRank, position + 1);
            String name = player.name;
            tvPlayerName.setText(name);
            tvScore.setText(String.valueOf(player.highScore));
            itemView.setOnClickListener(view -> listener.onItemClick(position));
        }
    }

    private void setTextForRank(TextView textView, ImageView imgTopRank, int i) {
        setTextForRankWithPrize(textView, imgTopRank, i);
    }

    @NonNull
    private void setTextForRankWithPrize(TextView textView, ImageView imgTopRank, int i) {
        textView.setText(String.valueOf(i));
        imgTopRank.setVisibility(View.GONE);
    }
}
