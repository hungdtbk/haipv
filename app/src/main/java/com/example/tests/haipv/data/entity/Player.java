package com.example.tests.haipv.data.entity;


import com.example.tests.haipv.utils.DateUtil;
import com.example.tests.haipv.utils.Utils;

import java.util.HashMap;

/**
 * Created by hungdt on 3/16/2018
 */

public class Player {
    public static final String FIELD_SCORE_BY_DATES = "scoreByDates";
    public String name;
    public int highScore;
    public int ruby = 0;
    public HashMap<String, Integer> scoreByDates = new HashMap<>();
    public String id;

    public Player() {
        id = Utils.getDeviceId();
        name = "";
        highScore = 0;
    }

    public Player(String name, int score) {
        id = Utils.getDeviceId();
        this.name = name;
        this.highScore = score;
    }

    public void writeScoreOfToDay(int score) {
        scoreByDates.put(DateUtil.getTodayString(), score);
    }

    public int scoreOfYesterday() {
        return Utils.getScoreByDate(scoreByDates, DateUtil.getYesterdayString());
    }
    public int scoreOfToDay() {
        return Utils.getScoreByDate(scoreByDates, DateUtil.getTodayString());
    }

    public int scoreOfWeek() {
        return Utils.getScoreByDate(scoreByDates, DateUtil.getThisWeekString());
    }

    public int scoreOfMonth() {
        return Utils.getScoreByDate(scoreByDates, DateUtil.getThisMonthString());
    }

    public void writeScoreOfWeek(int score) {
        scoreByDates.put(DateUtil.getThisWeekString(), score);
    }

    public void writeScoreOfMonth(int score) {
        scoreByDates.put(DateUtil.getThisMonthString(), score);
    }

    public void writeScoreOfYesterday(int score) {
        scoreByDates.put(DateUtil.getYesterdayString(), score);
    }
}
