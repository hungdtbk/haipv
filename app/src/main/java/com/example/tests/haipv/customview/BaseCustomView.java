package com.example.tests.haipv.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;

/**
 * Created by MSI on 9/29/2017.
 */

public abstract class BaseCustomView extends RelativeLayout {
    public BaseCustomView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(getLayoutResource(), this, true);
        ButterKnife.bind(this);
        initView();
        initListener();
    }

    public BaseCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(getLayoutResource(), this, true);
        ButterKnife.bind(this);
        initAttrs(attrs);
        initView();
        initListener();
    }

    protected abstract int getLayoutResource();
    protected void initAttrs(AttributeSet attrs) {
        /**Todo : override this method, follow this example :
         TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.ItemCountryDetail, 0, 0);
         try {
         mTitleText = ta.getString(R.styleable.ItemCountryDetail_title);
         } finally {
         ta.recycle();
        }*/
    }
    protected abstract void initView();

    protected void initListener() {

    }

}
