package com.example.tests.haipv.common;

/**
 * Created by Nguyen Dang Hung on 2/2/2017.
 */

public abstract class ApiListener<T> {
    public abstract void onSuccess(T result);

    public void onFailure(String errorMessage) {

    }

    public void onFailure(T fail, String errorMessage){

    }
}
