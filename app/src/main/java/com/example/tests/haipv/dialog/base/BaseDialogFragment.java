package com.example.tests.haipv.dialog.base;

import android.os.Handler;

/**
 * Created by MSI on 11/23/2017.
 */

public class BaseDialogFragment extends NoTitleDialogFragment {
    @Override
    public void dismiss() {
        new Handler().postDelayed(() -> {
            try {
                super.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 150);
    }
}
