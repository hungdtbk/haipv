package com.example.tests.haipv.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.tests.haipv.MyApplication;


/**
 * Created by hungdt on 5/14/2018
 */
public class NetWorkUtils {
    public static boolean isConnectInternet() {
        return isConnectInternet(MyApplication.getInstance());
    }
    private static boolean isConnectInternet(Context context) {
        if(context == null)
            return false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        } else {
            return false;
        }
        return false;
    }
}
