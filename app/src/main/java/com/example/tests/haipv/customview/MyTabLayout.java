package com.example.tests.haipv.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.example.tests.haipv.R;

import butterknife.BindView;
import butterknife.OnClick;

public class MyTabLayout extends BaseCustomView {
    @BindView(R.id.tv_tab_1)
    TextView tvTab1;
    @BindView(R.id.tv_tab_2)
    TextView tvTab2;
    @BindView(R.id.tv_tab_3)
    TextView tvTab3;

    /**
     * implement this function to set background for selected tab(custom background with selected state for 3 textviews)
     *
     * @param position
     */
    public void setSelectedTab(int position) {
        View[] tabs = {tvTab1, tvTab2, tvTab3};
        for (int i = 0; i <= 2; i++) {
            tabs[i].setSelected(i == position);
        }
    }

    public void setTitleTab3(String title) {
        tvTab3.setText(title);
    }

    public interface OnTabSelected {
        void onTabSelected(View view, int position);
    }

    public OnTabSelected listener;

    public MyTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.tab_layout_main;
    }

    @Override
    protected void initView() {
        setSelectedTab(0);
    }

    @OnClick({R.id.tv_tab_1, R.id.tv_tab_2, R.id.tv_tab_3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_1:
                listener.onTabSelected(view, 0);
                break;
            case R.id.tv_tab_2:
                listener.onTabSelected(view, 1);
                break;
            case R.id.tv_tab_3:
                listener.onTabSelected(view, 2);
                break;
        }
    }

    public void setListener(OnTabSelected listener) {
        this.listener = listener;
    }
}
