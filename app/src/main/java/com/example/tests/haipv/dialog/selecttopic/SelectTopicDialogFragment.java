package com.example.tests.haipv.dialog.selecttopic;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.example.tests.haipv.common.OnItemClickListener;
import com.example.tests.haipv.ui.main.MainViewModel;
import com.example.tests.haipv.R;
import com.example.tests.haipv.data.entity.Topic;
import com.example.tests.haipv.dialog.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SelectTopicDialogFragment extends BaseDialogFragment implements OnItemClickListener {
    @BindView(R.id.rcv_select_topic)
    RecyclerView rcvSelectTopic;
    Unbinder unbinder;
    private MainViewModel activityViewModel;
    private SelectTopicAdapter adapter;

    public interface OnTopicSelectListener{
        void onTopicSelected(Topic topic);
    }

    public OnTopicSelectListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_select_topic, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.TOP | Gravity.RIGHT);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activityViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        subscribeData();
    }

    private void subscribeData() {
        rcvSelectTopic.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new SelectTopicAdapter(activityViewModel.listTopicsToSelect);
        rcvSelectTopic.setAdapter(adapter);
        adapter.setListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setListener(OnTopicSelectListener listener) {
        this.listener = listener;
    }

    @Override
    public void onItemClick(int position) {
        listener.onTopicSelected(activityViewModel.listTopicsToSelect.get(position));
        dismiss();
    }
}
