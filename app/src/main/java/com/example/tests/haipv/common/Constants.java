package com.example.tests.haipv.common;

/**
 * Created by hungdt on 8/13/2018
 */
public interface Constants {
    String KEY_PLAYER = "KEY_PLAYER";
    String KEY_HIGHEST_SCORE = "KEY_HIGHEST_SCORE";
    String KEY_USER_RATED_APP = "KEY_USER_RATED_APP";
    String FB_COLLECTION_PLAYERS = "players";
    String FB_COLLECTION_COMMENT = "comments";
    public final String PRODUCT_ID1 = "ruby_pack_1";
    public final String PRODUCT_ID2 = "ruby_pack_2";
    public final String PRODUCT_ID3 = "ruby_pack_3";
    public final String PRODUCT_ID4 = "ruby_pack_4";
    public final String PRODUCT_ID5 = "ruby_pack_5";
    public final String PRODUCT_ID6 = "ruby_pack_6";
    String KEY_RANKING_REWARDED = "KEY_RANKING_REWARDED";
     String KEY_MISSION_COMPLETED = "daily_mission_completed";
    String KEY_MISSION_GONE = "daily_mission_gone";

    interface Configs{
        String BACKGROUND_SOUND = "london_bridge_instrumental_32_kb";
        int BONUS_RUBY_SHOW_INTERSTITIAL_ADS = 8;
        int BONUS_RUBY_WATCH_VIDEO = 20;
        int BONUS_RUBY_SHARE_FB = 50;
        String DEFAULT_USER_NAME = "New User";
        int MIN_PLAYER_NAME_LENGTH = 3;
    }
}
