package com.example.tests.haipv.ui.main;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.tests.haipv.data.entity.Player;
import com.example.tests.haipv.data.entity.Topic;
import com.example.tests.haipv.utils.Utils;

import java.util.List;

public class MainViewModel extends AndroidViewModel {
    public MutableLiveData<Topic> currentTopic;
    public List<Topic> listTopicsToSelect;
    public Player player;

    public MainViewModel(@NonNull Application application) {
        super(application);
        currentTopic = new MutableLiveData<>();
    }
}
