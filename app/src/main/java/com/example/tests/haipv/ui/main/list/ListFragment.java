package com.example.tests.haipv.ui.main.list;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tests.haipv.R;
import com.example.tests.haipv.common.ApiListener;
import com.example.tests.haipv.common.EndlessRecyclerViewScrollListener;
import com.example.tests.haipv.common.OnItemClickListener;
import com.example.tests.haipv.data.entity.Player;
import com.example.tests.haipv.data.entity.Topic;
import com.example.tests.haipv.firebase.FireBaseDBHelper;
import com.example.tests.haipv.ui.main.MainActivity;
import com.example.tests.haipv.ui.main.MainViewModel;
import com.example.tests.haipv.utils.LogUtils;
import com.example.tests.haipv.utils.Utils;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnItemClickListener {
    Unbinder unbinder;
    @BindView(R.id.rcv_list)
    RecyclerView rcvList;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private MainViewModel activityViewModel;
    private Topic topic;
    private ListAdapter adapter;

    List<Player> listPlayers = new ArrayList<>();
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private DocumentSnapshot lastVisible;

    public ListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activityViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        subscribeData();
    }

    private void subscribeData() {
        topic = activityViewModel.currentTopic.getValue();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rcvList.setLayoutManager(layoutManager);
        adapter = new ListAdapter();
        adapter.setListener(this);
        rcvList.setAdapter(adapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                ListFragment.this.onLoadMore(page, totalItemsCount, view);
            }
        };
        rcvList.addOnScrollListener(endlessRecyclerViewScrollListener);
        swipeRefreshLayout.setOnRefreshListener(this);

        getDataFromServer();
    }

    private void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        getDataFromServer();
    }

    /**
     * xem hướng dẫn về load data theo page của cloudfirestore ở đây:
     * https://firebase.google.com/docs/firestore/query-data/query-cursors
     */
    private void getDataFromServer() {
        LogUtils.logD("ListFragment", "getDataFromServer : ", topic.title);
        Toast.makeText(getContext(), "getting data", Toast.LENGTH_SHORT).show();
        FireBaseDBHelper.getDataFromServer(topic, lastVisible, new ApiListener<List<DocumentSnapshot>>() {
            @Override
            public void onSuccess(List<DocumentSnapshot> result) {
                Toast.makeText(getContext(), "success", Toast.LENGTH_SHORT).show();
                listPlayers.addAll(Utils.convertListPlayersFromDocumentSnapshot(result));
                adapter.updateData(listPlayers);
                lastVisible = result.get(result.size() - 1);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * called on pull to refresh
     */
    @Override
    public void onRefresh() {
        listPlayers.clear();
        adapter.notifyDataSetChanged();
        endlessRecyclerViewScrollListener.resetState();
        lastVisible = null;
        getDataFromServer();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(int position) {
        Player player = listPlayers.get(position);
        ((MainActivity)getActivity()).showDetail(player);
    }
}
